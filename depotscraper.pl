#!/bin/perl
use strict;
use warnings;
use IO::File;
use LWP::UserAgent;
use HTTP::Request::Common qw(GET);
use HTTP::Request::Common qw(POST);

# Initialize the user agent that will make our requests
my $ua = LWP::UserAgent->new();

# Show HTTP requests on the command line
$ua->show_progress('true value');

# Use the url for the main catalog page. Include Ann Arbor's store id
my $base_url = "http://www.homedepot.com/webapp/catalog/servlet/ContentView?pn=Tool_Truck_Rental&langId=-1&storeId=10051&catalogId=10053";
# Define our request
my $req = GET $base_url;

# Make the request and store the decoded html result
my $content = $ua->request($req)->decoded_content();

# A hash eliminates the need to check for duplicate listings
my %toolPages;
# Search for tool urls with regex
while ( $content =~ /(homedepotrents.com\/(proTools|diyTools)\/.*?\.asp)/g ) {
  # Store found url in our hash
  $toolPages{$1}{found} = 1;
}

# Scrape any
foreach (keys %toolPages) {
  my $toolUrl = $_;
  my $toolReq = GET "http://www.$toolUrl";
  my $toolContent = $ua->request($toolReq)->decoded_content();
  my $baseUrl;
  if ( $toolContent =~ /top25diy\.gif/ ) {
    $baseUrl = "homedepotrents.com/diyTools/";
  } else {
    $baseUrl = "homedepotrents.com/proTools/";
  }
  while ( $toolContent =~ /<a href=\"(.*?\.asp)/g ) {
    $toolPages{"$baseUrl$1"}{found} = 1;
  }
}

# Iterate over the urls we collected
foreach (keys %toolPages) {
  #print "$_\n";
  my $toolUrl = $_;
  my $toolReq = GET "http://www.$toolUrl";
  # Get the html contents of each tool's page
  my $toolContent = $ua->request($toolReq)->decoded_content();
  
  # Find and store the tool's image
  if ( $toolContent =~ /<img src=\"\.\.\/(images\/.*?)\"/ ) {
    my $img = "http://www.homedepotrents.com/$1";
    $toolPages{$_}{img} = $img;
  }
  # Name
  if ( $toolContent =~ /<h1>(.*?)<\/h1>/ ) {
    my $name = "$1";
    $toolPages{$_}{name} = $name;
  }
  # Subname
  if ( $toolContent =~ /<h2>(.*?)<\/h2>/ ) {
    my $subname = "$1";
    $toolPages{$_}{subname} = $subname;
  }
  # Description
  while ( $toolContent =~ /<ul>\s*((.*\s*)*?)\s*?<\/ul>/g ) {
    # Make sure the <ul> isn't just a list of tools
    if ( index($1, "a href") == -1 ) {
      my $description = $1;
      # Replace html markup
      $description =~ s/(<li>|<\/li>)//g;
      $description =~ s/\&quot\;/\"/g;
      $description =~ s/\&trade\;//g;
      $description =~ s/\&deg\;/°/g;
      $description =~ s/\&reg\;//g;
      $description =~ s/\t//g;
      $description =~ s/<\/p>//g;
      $description =~ s/<p>//g;
      $description =~ s/ +/ /g;
      $description =~ s/\n+/\. /g;
      $description =~ s/ \r//g;
      $description =~ s/\r//g;
      $description =~ s/ \.//g;
      
      $toolPages{$_}{description} = $description;
    }
  }
}

# Print what we've found
foreach (keys %toolPages) {
  my $json =  <<END;
  {
    title: \"$toolPages{$_}{name}\",
    description: \"$toolPages{$_}{description}\",
    price: rand(50),
    address: Faker::Address.street_address,
    gmaps: 't',
    user_id: rand(20),
    img_url: \"$toolPages{$_}{img}\",
    city: Faker::Address.city,
    state: Faker::Address.state_abbr,
    zipcode: Faker::Address.zip_code,
    security_deposit: rand(100)
  },
END
  print $json;
}

my $count = keys %toolPages;
print "Scraped $count tools.\n";

exit;
